import os.path
import csv
#from datetime import datetime
import dateutil.parser


project_name= "wp-calypso"
pr_file_path= os.path.join(os.path.dirname(__file__), project_name, "1-" + project_name + ".csv")
comments_file_path= os.path.join(os.path.dirname(__file__), project_name, "2-" + project_name + ".csv")
commits_file_path=os.path.join(os.path.dirname(__file__), project_name, "3-" + project_name + ".csv")
fulfilled_file_path=os.path.join(os.path.dirname(__file__), project_name, "4-" + project_name + "-fulfilled-by-original-author.csv")

class pull_request(object):
    """__init__() functions as the class constructor"""
    def __init__(self, PullRequestNumber=None, user_login=None):
        self.PullRequestNumber = PullRequestNumber
        self.user_login = user_login

class comment(object):
    """__init__() functions as the class constructor"""
    def __init__(self, PullRequestNumber=None, created_at=None):
        self.PullRequestNumber = PullRequestNumber
        self.created_at = created_at

class commit(object):
    """__init__() functions as the class constructor"""
    def __init__(self, PullRequestNumber=None, commit_author_name=None, commit_author_date=None):
        self.PullRequestNumber = PullRequestNumber
        self.commit_author_name = commit_author_name
        self.commit_author_date = commit_author_date

class fulfilled_by_original_author(object):
    """__init__() functions as the class constructor"""
    def __init__(self, PullRequestNumber=None, isFulfilled=None):
        self.PullRequestNumber = PullRequestNumber
        self.isFulfilled = isFulfilled


pull_request_list = []
with open(pr_file_path, mode='r') as pr_1:
    csv_file_reader = csv.reader(pr_1, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE, escapechar="\\")
    headers = next(csv_file_reader)   #skip the header
    for csv_reader in csv_file_reader:
        pull_request_list.append(pull_request(csv_reader[0].strip('\"'),csv_reader[1].strip('\"')))

comment_list = []
with open(comments_file_path, mode='r') as comment_2:
    csv_file_reader = csv.reader(comment_2, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE, escapechar="\\")
    headers = next(csv_file_reader)   #skip the header
    for csv_reader in csv_file_reader:
        comment_list.append(comment(csv_reader[2].strip('\"'),csv_reader[1].strip('\"')))

commit_list = []
with open(commits_file_path, mode='r') as commit_3:
    csv_file_reader = csv.reader(commit_3, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE, escapechar="\\")
    headers = next(csv_file_reader)   #skip the header
    for csv_reader in csv_file_reader:
        commit_list.append(commit(csv_reader[2].strip('\"'),csv_reader[0].strip('\"'),csv_reader[1].strip('\"')))  


fulfilled_list = []
count_fulfilled = 0
count_not_fulfilled = 0

for current_pr in pull_request_list:
    pull_req_number = current_pr.PullRequestNumber
    pull_req_author = current_pr.user_login
    last_comment_timestamp = None
    #last_commit_by_original_author_timestap = None
    fulfilled = False
    for current_comment in comment_list:
        if current_comment.PullRequestNumber != pull_req_number:
            continue
        else:
            if (last_comment_timestamp is None) or (last_comment_timestamp < dateutil.parser.parse(current_comment.created_at.strip('\"'))):
                last_comment_timestamp = dateutil.parser.parse(current_comment.created_at.strip('\"'))
                
    for current_commit in commit_list:
        if current_commit.PullRequestNumber != pull_req_number:
            continue
        else:
            if (current_commit.commit_author_name == pull_req_author) and (dateutil.parser.parse(current_commit.commit_author_date.strip('\"')) > last_comment_timestamp):
                fulfilled = True
                count_fulfilled = count_fulfilled + 1
                break
    
    fulfilled_list.append(fulfilled_by_original_author(pull_req_number, fulfilled))

count_total = len(fulfilled_list)
count_not_fulfilled = count_total - count_fulfilled
with open(fulfilled_file_path, mode='w') as fulfilled_4:
    csv_file_writer = csv.writer(fulfilled_4, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE, escapechar="\\")
    #csv_file_writer.writerow(headers)
    for current_fulfilled in fulfilled_list:
        csv_file_writer.writerow([current_fulfilled.PullRequestNumber, current_fulfilled.isFulfilled])
    
    csv_file_writer.writerow([])
    csv_file_writer.writerow(["","Total: ", count_total])
    csv_file_writer.writerow(["","Fulfilled by original author: ", count_fulfilled, "{0:.2f}".format( (count_fulfilled/count_total))])
    csv_file_writer.writerow(["","Not fulfilled by original author: ", count_not_fulfilled, "{0:.2f}".format( (count_not_fulfilled/count_total))])
print("done for project "+ project_name)

