# https://bigquery.cloud.google.com/table/githubarchive:year.2017

SELECT
  repo.name,
  COUNT(*) c,
  COUNT(DISTINCT actor.id) authors,
FROM
  [githubarchive:month.201812],
  [githubarchive:month.201811],
  [githubarchive:month.201810],
  [githubarchive:month.201809],
  [githubarchive:month.201808],
  [githubarchive:month.201807],
  [githubarchive:month.201806],
  [githubarchive:month.201805],
  [githubarchive:month.201804],
  [githubarchive:month.201803],
  [githubarchive:month.201802],
  [githubarchive:month.201801],
  [githubarchive:year.2017]
WHERE
  type IN ( 'PullRequestEvent')
  AND JSON_EXTRACT(payload, '$.action') IN ('"opened"')
GROUP BY
  repo.name
ORDER BY
  c DESC
